### Trees with C

**List/Unary Tree**

list.c

gcc list.c -o bin/list && ./bin/list


**Binary Tree**

btree.c

gcc btree.c -o bin/btree && ./bin/btree

**AVL Tree**

gcc avltree.c -o bin/avltree && ./bin/avltree

<br>

**TODO:**
- ADT Graph
- String AVL-Tree