/*
Implementation of a simple linked list data structure in C.

Functions operate destructive.

AUTHOR: Sean Pedersen

TODO:
- Create empty list type with length attribute
- Create test cases

*/

#include <string.h>
#include <stdlib.h>
#include <stdio.h>

typedef struct list_node {
    int value;
    struct list_node* next;
} list_node;


list_node* create(int value) {
    // Create a list with a single element (value)
    list_node* new_list_node = (list_node*)malloc(sizeof(list_node));
    if (new_list_node == NULL) {
        printf("Error allocating memory.\n");
        exit(0);
    }
    new_list_node->value = value;
    new_list_node->next = NULL;

    return new_list_node;
}

void delete_at(list_node* head, int index) {
    // Deletes element at index
    
    list_node* prior = NULL;
    list_node* current = head;
    int i = 0;
    while (current != NULL) {
        if (i == index) {
            if (prior != NULL) {
                prior->next = current->next;
                free(current);
            }
            else {
                if (head->next != NULL)
                    *head = *head->next;
                else
                    head = NULL;
            }
            return;
        }
        current = current->next;
        prior = current;
        i++;
    }

    printf("INDEX ERROR\n");
}

int pop(list_node* head) {
    // Returns value of head element and removes it
    int result = head->value;
    delete_at(head, 0);
    return result;
}

int get(list_node* head, int index) {
    // Returns value of element at index

    list_node* current = head;
    int i = 0;
    while (current != NULL) {
        if (i == index)
            return current->value;
        current = current->next;
        i++;
    }

    printf("INDEX ERROR\n");
    //TODO: Deal with this exception
}

void prepend(list_node* head, int value) {
    // Add new value to start of list

    list_node* new_head = create(value);
    list_node* old_head = (list_node*)malloc(sizeof(list_node));
    memcpy(old_head, head, sizeof(list_node));
    new_head->next = old_head;
    *head = *new_head;
}

void append(list_node* head, int value) {
    // Add new value to end of list

    //printf("APPENDING %i\n", value);
    list_node* current = head;

    // Must be current->next, because otherwise current would be NULL..
    // losing the reference to list_node data structure
    while (current->next != NULL)
        current = current->next;
    
    current->next = create(value);
}

int search(list_node* head, int value) {
    // Search for value in list, return index of value or -1 if not found

    list_node* current = head;
    int i = 0;
    while (current != NULL) {
        if (value == current->value)
            return i;
        current = current->next;
        i++;
    }
    return -1;
}

void print_list(list_node* head) {
    // Print all values of a list (iterative)
    list_node* current = head;

    while (current != NULL) {
        printf("%d\n", current->value);
        current = current->next;
    }
    printf("ENDE\n");
}

void printL(list_node* head) {
    // Print all values of a list (recursive)
    if (head != NULL) {
        printf("%d\n", head->value);
        printL(head->next);
    }
    else {
        printf("ENDE\n");
    }
}

int main(int argc, char** argv) {
    list_node* head = create(1);

    prepend(head, 2);
    prepend(head, -2);
    prepend(head, -32);
    prepend(head, 3);
   // delete_at(head, 0);

    printL(head);

    printf("POP: %i\n", pop(head));
    printf("POP: %i\n", pop(head));
   // delete_at(head, 0);
   
    printL(head);

    printf("Position von 1: %i\n", search(head, 1));
    printf("Position von -2: %i\n", search(head, -2));
    printf("Position von 3: %i\n", search(head, 3));
    printf("Position von 0: %i\n", search(head, 0));
    printf("GET index 1: %i\n", get(head, 1));
}