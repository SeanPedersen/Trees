/*
Implementation of a simple binary search tree in C.
AUTHOR: Sean Pedersen

TODO:
- create a traverse tree func which takes a func as param
*/

//TO REMEMBER: Clean up malloc vars on removal!!!
#include <stdlib.h>
#include <stdio.h>


/////////////////////
// Struct definitions
/////////////////////
typedef struct tree_node {
    int value;
    struct tree_node* left;
	struct tree_node* right;
} tree_node;


/////////////////////
// Binary Tree Functions
/////////////////////

tree_node* create_node(int value) {
    tree_node* tree = NULL;
    if( ( tree = malloc( sizeof( tree_node ) ) ) == NULL ) {
		printf("Error allocating memory for new node.\n");
        exit(0);
	}
    tree->value = value;
    tree->left = NULL;
    tree->right = NULL;
    return tree;
}

void insert(tree_node* tree, int insert_value) {
    /* Insert a new node by insert_value. */
    
    printf("Inserting: %i\n", insert_value);
    tree_node* current_node = tree;
    tree_node* last_node = NULL;
    int current_node_value;

    while (current_node != NULL) {
        last_node = current_node;
        current_node_value = current_node->value;       
        printf("%i\n", current_node_value);
        // Use iteration in C not recursion (more efficient)
        if (insert_value < current_node_value) {
            printf("Go Left\n");
            current_node = current_node->left;
        }
        else {
            printf("Go Right\n");
            current_node = current_node->right;
        }
    }

    if( insert_value < last_node->value )
        last_node->left = create_node(insert_value);
    else
        last_node->right = create_node(insert_value);
}

int search(tree_node* tree, int value) {
    // Search for value in tree, return depth of value or -1 if not found

    tree_node* current_node = tree;
    int current_node_value;
    int depth = 0;

    while (current_node != NULL) {
        current_node_value = current_node->value;
        depth++;
        // Use iteration in C not recursion (more efficient)
        if (value == current_node_value) {
            return depth;
        }
        else if (value < current_node_value) {
            current_node = current_node->left;
        }
        else {
            current_node = current_node->right;
        }
    }
    return -1;
}

void printBT(tree_node* tree_node) {
    if (tree_node != NULL) {
        printf("%i\n", tree_node->value);
        printf("Left\n");
        printBT(tree_node->left);
        printf("Right\n");
        printBT(tree_node->right);
    }
    else {
        printf("NULL\n");
    }
}


int main(int argc, char** argv) {
    tree_node* root = create_node(33);
    insert(root, 42);
    insert(root, 24);
    insert(root, 66);

    printf("Print BTREE:\n");
    printBT(root);
    printf("Search depth 66: %i\n", search(root, 66));
    printf("Search depth 42: %i\n", search(root, 42));
    printf("Search depth 33: %i\n", search(root, 33));
    printf("Search depth 1: %i\n", search(root, 1));

    return 0;
}
