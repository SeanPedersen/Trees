/*
Implementation of a self-balancing binary search tree (AVL) in C.
AUTHOR: Sean Pedersen
*/

//TO REMEMBER: Clean up malloc vars on removal!!!
#include <stdlib.h>
#include <stdio.h>
#include <string.h>


/////////////////////
// Struct definitions
/////////////////////
typedef struct tree_node {
    int value;
    int height;
    struct tree_node* left;
	struct tree_node* right;
} tree_node;

typedef struct list_node {
    // A list to save tree nodes
    struct tree_node* value;
    struct list_node* next;
} list_node;


/////////////////////
// List Functions
/////////////////////

list_node* create(tree_node* value) {
    // Create a list with a single element (value)
    list_node* new_list_node = (list_node*)malloc(sizeof(list_node));
    if (new_list_node == NULL) {
        printf("Error allocating memory.\n");
        exit(0);
    }
    new_list_node->value = value;
    new_list_node->next = NULL;

    return new_list_node;
}

void delete_at(list_node* head, int index) {
    // Deletes element at index
    
    list_node* prior = NULL;
    list_node* current = head;
    int i = 0;
    while (current != NULL) {
        if (i == index) {
            if (prior != NULL) {
                prior->next = current->next;
                free(current);
            }
            else {
                if (head->next != NULL)
                    *head = *head->next;
                else {
                    head = NULL;
                }
                    
            }
            return;
        }
        current = current->next;
        prior = current;
        i++;
    }

    printf("INDEX ERROR\n");
}

tree_node* pop(list_node* head) {
    // Returns value of head element and removes it
    tree_node* result = head->value;
    delete_at(head, 0);
    return result;
}

void prepend(list_node* head, tree_node* value) {
    // Add new value to start of list

    list_node* new_head = create(value);
    list_node* old_head = (list_node*)malloc(sizeof(list_node));
    memcpy(old_head, head, sizeof(list_node));
    new_head->next = old_head;
    *head = *new_head;
}

/////////////////////
// Binary Tree Functions
/////////////////////

int max(int a, int b) {
    if (a > b)
        return a;
    return b;
}

tree_node* create_node(int value) {
    tree_node* tree = NULL;
    if( ( tree = malloc( sizeof( tree_node ) ) ) == NULL ) {
		printf("Error allocating memory for new node.\n");
        exit(0);
	}
    tree->value = value;
    tree->height = 0;
    tree->left = NULL;
    tree->right = NULL;
    return tree;
}

void update_node_height(tree_node* node) {
    tree_node* left_node = node->left;
    tree_node* right_node = node->right;
    int left_height = 0;
    int right_height = 0;

    if (left_node != NULL)
        left_height = left_node->height;
    if (right_node != NULL)
        right_height = right_node->height;
    
    //printf("TEST: %i\n", right_node->height);
    node->height = max(left_height, right_height) + 1; // max(LH, RH) + 1
}

void insert(tree_node* tree, int insert_value) {
    /* Insert a new node by insert_value. */
    
    printf("Inserting: %i\n", insert_value);
    tree_node* current_node = tree;
    tree_node* last_node = NULL;
    list_node* visited_nodes_stack = create(current_node);
    int current_node_value;

    while (current_node != NULL) {
        prepend(visited_nodes_stack, current_node);
        last_node = current_node;
        current_node_value = current_node->value;       
        printf("Current Val: %i\n", current_node_value);
        printf("Current Height: %i\n", current_node->height);
        // Use iteration in C not recursion (more efficient)
        if (insert_value < current_node_value) {
            printf("Go Left\n");
            current_node = current_node->left;
        }
        else {
            printf("Go Right\n");
            current_node = current_node->right;
        }   
    }

    if( insert_value < last_node->value )
        last_node->left = create_node(insert_value);
    else
        last_node->right = create_node(insert_value);
    
    while (visited_nodes_stack->next != NULL) {
        update_node_height(pop(visited_nodes_stack));
    }
    free(visited_nodes_stack);
}

int search(tree_node* tree, int value) {
    // Search for value in tree, return depth of value or -1 if not found

    tree_node* current_node = tree;
    int current_node_value;
    int depth = 0;

    while (current_node != NULL) {
        current_node_value = current_node->value;
        depth++;
        // Use iteration in C not recursion (more efficient)
        if (value == current_node_value) {
            return depth;
        }
        else if (value < current_node_value) {
            current_node = current_node->left;
        }
        else {
            current_node = current_node->right;
        }
    }
    return -1;
}

void printBT(tree_node* tree_node) {
    if (tree_node != NULL) {
        printf("VAL: %i\n", tree_node->value);
        printf("HEIGHT: %i\n", tree_node->height);
        printf("Left\n");
        printBT(tree_node->left);
        printf("Right\n");
        printBT(tree_node->right);
    }
    else {
        printf("NULL\n");
    }
}


int main(int argc, char** argv) {
/*     tree_node* root = create_node(3);

    list_node* head = create(root);

    prepend(head, root); */


    tree_node* root = create_node(3);
    printf("\n\nPrint BALANCED B-TREE:\n\n");
    printBT(root);
    printf("\n");
    insert(root, 1);
    printf("\n\nPrint BALANCED B-TREE:\n\n");
    printBT(root);
    printf("\n");
    insert(root, 5);
    printf("\n\nPrint BALANCED B-TREE:\n\n");
    printBT(root);
    printf("\n");
    insert(root, 6);
    printf("\n\nPrint BALANCED B-TREE:\n\n");
    printBT(root);
    printf("\n");
    insert(root, 7);

    printf("\n\nPrint BALANCED B-TREE:\n\n");
    printBT(root);


    printf("Search depth 6: %i\n", search(root, 6));
    printf("Search depth 1: %i\n", search(root, 1));

    return 0;
}
